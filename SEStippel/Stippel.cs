﻿using Sandbox.Common;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Engine;
using Sandbox.Game;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces;

using SpaceEngineers.AI;
using SpaceEngineers.Game.ModAPI.Ingame;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage.Utils;
using VRage.Voxels;

using VRageMath;


namespace SEStippel
{
    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_PistonTop), false)]
    public class Stippel : MyGameLogicComponent 
    {
        IMyPistonTop stippel;
        IMyPlayer target;
        MyCubeGrid grid;
        Status status;
        List<IMyPlayer> playerList = new List<IMyPlayer>();
        double maxHuntingDistance = 20;
        double suctionDistance = 0.25;
        double maxIdleSpeed = 1;
        double maxHuntingSpeed = 2;
        int crawlingState = 0;

        private enum Status
        {
            Idle, Hunting
        }

        public override void Init(MyObjectBuilder_EntityBase objectBuilder)
        {
            NeedsUpdate = MyEntityUpdateEnum.BEFORE_NEXT_FRAME;
            //base.Init(objectBuilder); REMOVE
        }

        public override void UpdateOnceBeforeFrame()
        {
            status = Status.Idle;
            stippel = Entity as IMyPistonTop;
            grid = stippel.CubeGrid as MyCubeGrid;

            //stippel.CustomName = "Stippel";
            //MyAPIGateway.Utilities.ShowNotification($"{stippel.CustomName} started working!", 1000);
            NeedsUpdate = MyEntityUpdateEnum.EACH_FRAME | MyEntityUpdateEnum.EACH_100TH_FRAME;
        }

        public override void UpdateAfterSimulation()
        {
            if (stippel == null || stippel.MarkedForClose || stippel.Closed) return;
            if (grid.Physics == null || grid.Physics.IsStatic) return;

            switch (status)
            {
                case Status.Hunting:
                    Hunt();
                    break;
                default:
                    Idle();
                    break;
            }
        }

        private void Idle()
        {
            Vector3D castStart = stippel.GetPosition() + 0.8 * stippel.WorldMatrix.Up;
            Vector3D castDestination = castStart - stippel.WorldMatrix.Up * suctionDistance;
            IHitInfo hitInfo;

            //MyAPIGateway.Utilities.ShowMessage("Stippel", "Cast out!");
            if (MyAPIGateway.Physics.CastRay(castStart, castDestination, out hitInfo))
            {
                //MyAPIGateway.Utilities.ShowMessage("Stippel", "Cast hit!");
                //MyTransparentGeometry.AddLineBillboard(MyStringId.GetOrCompute("Square"), Color.Red * 0.5f, castStart, castDestination - castStart, 1, 0.05f);

                double distance = (castStart - hitInfo.Position).Length();
                //MyAPIGateway.Utilities.ShowMessage("Stippel", distance.ToString());
                if (distance < suctionDistance)
                {
                    Vector3 force = -stippel.WorldMatrix.Up * 5000;
                    force = stippel.WorldMatrix.Forward * 3000;
                    MaxForce(ref force);

                    Vector3D force_acting_position = stippel.GetPosition();
                    Vector3 torque = new Vector3D(0, 0, 0);
                    grid.Physics.AddForce(MyPhysicsForceType.APPLY_WORLD_FORCE, force, force_acting_position, torque);
                }
            }
        }

        private void Hunt()
        {
            Vector3D force_acting_position = stippel.GetPosition();
            Vector3 force = Vector3D.Normalize(target.GetPosition() - stippel.GetPosition()) * 10000;
            Vector3 torque = new Vector3D(0, 0, 0);

            grid.Physics.AddForce(MyPhysicsForceType.APPLY_WORLD_FORCE, force, force_acting_position, torque);
        }

        private void MaxForce(ref Vector3 force)
        {
            crawlingState += 3;
            crawlingState %= 180;

            double velocity = grid.Physics.GetVelocityAtPoint(grid.Physics.CenterOfMassWorld).Length() * Math.Sin(Math.PI * crawlingState / 180);
            if (velocity > maxIdleSpeed)
            {
                force = Vector3.Zero;
            }
        }

        public override void UpdateAfterSimulation100()
        {
            UpdateStatus();
        }

        private void UpdateStatus()
        {
            target = GetNearestPlayer();
            if (target != null)
            {
                //status = Status.Hunting;
            }
            else
            {
                status = Status.Idle;
            }
        }

        private IMyPlayer GetNearestPlayer()
        {
            MyAPIGateway.Players.GetPlayers(playerList);
            double currentDistance = 0;
            IMyPlayer nearestPlayer = null;
            double minDistance = maxHuntingDistance;

            foreach (IMyPlayer player in playerList)
            {
                currentDistance = (stippel.GetPosition() - player.GetPosition()).Length();
                if (currentDistance < minDistance)
                {
                    nearestPlayer = player;
                    minDistance = currentDistance;
                }
            }
            return nearestPlayer;
        }

        public override void Close()
        {
            stippel = null;
        }
    }
}
